#include <stdio.h>
#include <ctype.h>

int main(void)
{
    int wordcount = 0; // 총 단어 개수
    int ulwdcount = 0; // 대문자로 시작해 소문자로 끝나는 단어 개수
    char a, b, c, d;
    int i, j;
    i = 1;
    j = 1;
    a = getchar();
    while(EOF != a)
    {
        if(i == 1)
        {
            if(!isspace(a))
            {
                if(j == 1)
                {
                    if(isupper(a))
                    {
                        j = 2;
                    }
                }
                i = 2;
            }

        }else
        {
            if(j == 2 || j == 3)
            {
                if(!isspace(a))
                {
                    if(islower(a))
                    {
                        j = 2;
                    }else
                    {
                        j = 3;
                    }
                }else
                {
                    if(j == 2)
                    {
                        ++ulwdcount;
                        j = 1;
                    }else
                    {
                        j = 1;
                    }
                }
            }
            if(isspace(a))
            {
                ++wordcount;
                i = 1;
            }
        }
        a = getchar();
    }
    printf("%d\n", wordcount);
    printf("%d\n", ulwdcount);
    return 0;
}
